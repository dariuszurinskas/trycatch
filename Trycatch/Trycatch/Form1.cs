﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trycatch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float a = 0;
            float b = 0;
            float c;
          
            try
            {
                a = Convert.ToSingle(text1.Text);               
            }
            catch
            {

                text3.Text = "blogai ivedete Pirma skaicius";
                return;
            }


            try
            {
                b = Convert.ToSingle(text2.Text);
            }
            catch 
            {
                text3.Text ="blogai ivedete Antra skaicius";
                return;
            }

            c = a + b;

            text3.Text = c.ToString();
            

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
